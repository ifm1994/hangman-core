const getCurrentWordFromHistory = require('./get-current-word-from-history');
const getErrorsFromHistory = require('./get-errors-from-history');
const getAscii = require('./get-ascii');
const getScore = require('./get-score');
const getRemainingTime = require('./get-remaining-time');

module.exports = function getStateFromHistory(word, history, timer) {
  return {
    ascii: getAscii(getErrorsFromHistory(word, history)),
    numberOfLetters: word.replace(/\s/g, '').length,
    currentState: getCurrentWordFromHistory(word, history),
    errorsNumber: getErrorsFromHistory(word, history),
    score: getScore(word, history),
    history,
    remainingtime: timer.getRemainingTime(),
    word
  }
}
