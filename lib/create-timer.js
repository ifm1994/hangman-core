module.exports = function createTimer(time, onTimeUp){
    const started = Date.now();
    let timeOut;

    if(onTimeUp){
        timeOut = setTimeout(onTimeUp, time);
    }

    return {
        clear: () => { clearTimeout(timeOut) },
        getRemainingTime: () => Math.max(time - (Date.now() - started), 0)
    }
}