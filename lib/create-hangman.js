const pickFromArray = require('./pick-random-from-array');
const getStateFromHistory = require('./get-state-from-history');
const createTimer = require('./create-timer');
const DEFAULT_TIME = 60 * 1000;

module.exports = function createHangman(options) {
  let history, word, timer;

  const initialize = () => {
    if(timer){
      timer.clear();
    }
    timer = createTimer(options.time || DEFAULT_TIME, options.onTimeUp);
    word = pickFromArray(options.words);
    history = [];
  }

  const play = letter => {
    if (letter) { history.push(letter); }
    return getStateFromHistory(word, history, timer);
  }

  initialize();

  return { play, reset: initialize };
}