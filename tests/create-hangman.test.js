const createHangman = require('../lib/create-hangman');

describe('Hangman game', () => {
  let hangman

  beforeAll(() => {
    hangman = createHangman({
      words: ['this is a test'],
      onTimeDone: () => console.log('El tiempo se ha agotado'),
      time: 5000
    }
  );

  test('returns the original state2', () => {
    expect(hangman.play()).toMatchObject({
      remainingTime: 0,
      numberOfLetters: 11,
      currentState: '__  _ ____',
      errorsNumber: 0,
      history: [],
      word: 'this is a test',
      score: 100
    })  
  })
  
  test('returns the original state', () => {
    expect(hangman.play()).toMatchObject({
      numberOfLetters: 11,
      currentState: '____ __ _ ____',
      errorsNumber: 0,
      history: [],
      word: 'this is a test',
      score: 100
    })  
  })

  test('returns the right state after playing', () => {
    hangman.play('e')
    hangman.play('c')

    expect(hangman.play()).toMatchObject({
      numberOfLetters: 11,
      currentState: '____ __ _ _e__',
      errorsNumber: 1,
      history: ['e', 'c'],
      score: 90,
      word: 'this is a test'
    })  
  })
})
